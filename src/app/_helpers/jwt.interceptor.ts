import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Assuming you have the authentication token stored somewhere
    const authToken = 'snjdu438fkdj38fdmcv7dm3ckvhrsnjdu438fkdj38fdmcv7dm3ckvhr';

    // Clone the request and add the Authorization header
    const modifiedRequest = req.clone({
      setHeaders: {
        Authorization: `Bearer ${authToken}`
      }
    });

    return next.handle(modifiedRequest);
  }
}
