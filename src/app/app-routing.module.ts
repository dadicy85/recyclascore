import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './_pages/auth/login/login.component';
import { RegisterComponent } from './_pages/auth/register/register.component';
import { ForgotPasswordComponent } from './_pages/auth/forgot-password/forgot-password.component';
import { BoardAdminComponent } from './_pages/auth/board-admin/board-admin.component';
import { BoardModeratorComponent } from './_pages/auth/board-moderator/board-moderator.component';
import { BoardUserComponent } from './_pages/auth/board-user/board-user.component';

const routes: Routes = [
  { path: 'inscription', component: RegisterComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'connexion', component: LoginComponent },
  { path: 'user', component: BoardUserComponent },
  { path: 'mod', component: BoardModeratorComponent },
  { path: 'admin', component: BoardAdminComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
