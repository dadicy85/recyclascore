export class Object {
  id!: Number;
  name!: String;
  material!: String;
  recyclabilityIndex!: Number;
}
