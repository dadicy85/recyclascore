export class User {
    id!: number;
    username!: string;
    password!: string;
    email!: string;
    firstname!: string;
    lastname!: string;
    address!: string;
    phone!: string;
    objects!: any[];
  }

