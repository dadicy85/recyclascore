import { Injectable } from '@angular/core';
import { jwtDecode } from 'jwt-decode';

const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() {}

  clean(): void {
    localStorage.clear();
  }

  // 1/ Récupère le Token via localStorage
  public getToken(): string | null {
    return localStorage.getItem('token')
  }

  // 2/ Décode du token récupéré via local Storage avec jwtDecode
  public decodeToken(token: string ){
    return jwtDecode(token)
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }

  public isLoggedIn(): boolean {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return true;
    }

    return false;
  }
}
