import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  private API_URL = 'http://localhost:8080/api/objects';

  constructor(private http: HttpClient) { }

  getObjects(): Observable<Object[]> {
    return this.http.get<Object[]>(`${this.API_URL}/getObjects`);
  }

  getRecyclabilityIndex(objectName: string): Observable<number> {
    return this.http.get<number>(`${this.API_URL}/recyclabilityIndex?name=${objectName}`);
  }
}
