import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';
import { User } from '../_models/User';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private AUTH_API = 'http://localhost:8080/api/auth';
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient, private router: Router, private storageService: StorageService) {}

  // Méthode pour effectuer la connexion
  login(userData: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      withCredentials: true  // Inclus les cookies dans la requête
    };

    // console.log(userData)
    return this.http.post<any>(`${this.AUTH_API}/login`, userData, options).pipe(
      tap(response => {
        if (response && response.token) {
          // Stockage du token JWT dans le stockage local
          localStorage.setItem('token', response.token);
          console.log('Connexion réussie');
          this.router.navigate(['/']);
        }
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 403) {
          // Gestion du cas où l'accès est interdit
          console.error('Accès interdit. Redirection vers la page de connexion.');
          // Redirection vers la page de connexion
          this.router.navigate(['/connexion']);
        } else {
          // Gestion des autres erreurs HTTP
          console.error('Erreur HTTP lors de la connexion :', error);
          alert('Erreur de connexion');
        }
        // Relancez l'erreur pour qu'elle soit traitée ailleurs
        return throwError(error);
      })
    );
  }

  isLoggedIn() {
    return localStorage.getItem('token') !== null;
  }

  // Méthode pour effectuer l'inscription
  register(userData: any): Observable<any> {
    return this.http.post<any>(`${this.AUTH_API}/register`, userData, this.httpOptions);
  }

  // Méthode pour effectuer la déconnexion
  logout() {
    return this.http.post<any>(`${this.AUTH_API}/logout`, {}, this.httpOptions).pipe(
      tap(res => {
        console.log(res);
        this.storageService.clean();
        // Redirection ou logique supplémentaire après la déconnexion réussie
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 403) {
          // Gérer le cas où l'accès est interdit (Forbidden)
          console.error('Accès interdit. Redirection vers la page de connexion.');
          // Redirection vers la page de connexion
          this.router.navigate(['/connexion']);
        }
        // Si ce n'est pas une erreur 403, relancez l'erreur pour qu'elle soit traitée ailleurs
        return throwError(error);
      })
    );
  }
}
