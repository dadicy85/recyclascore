import { AuthService } from '../../../_services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { RxwebValidators } from '@rxweb/reactive-form-validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  registerForm!: FormGroup;
  showAdditionalFields: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group ({
        username: ['', [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20)
        ]],
        email: ['', [
          Validators.required,
          Validators.email
        ]],
        password: ['', [
          Validators.required,
          Validators.minLength(8)
        ]],
        confirmPassword: ['', [
          RxwebValidators.compare({fieldName:'password'})
        ]],
        phone: ['', [
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern('^[0-9]*$')
        ]],
        firstname: ['', []],
        lastname: ['', []],
        address: ['', []]
      });
    }
      get username() {
        return this.registerForm.get('username') as FormControl;
      }
      get email() {
        return this.registerForm.get('email') as FormControl;
      }
      get password() {
        return this.registerForm.get('password') as FormControl;
      }
      get confirmPassword() {
        return this.registerForm.get('confirmPassword') as FormControl;
      }
      get firstname() {
        return this.registerForm.get('firstname') as FormControl;
      }
      get lastname() {
        return this.registerForm.get('lastname') as FormControl;
      }
      get address() {
        return this.registerForm.get('address') as FormControl;
      }
      get phone() {
        return this.registerForm.get('phone') as FormControl;
      }
      toggleAdditionalFields() {
        this.showAdditionalFields = !this.showAdditionalFields;
      }

      onSubmit() {
        if (this.registerForm.valid) {
          const userData = this.registerForm.value; // Récupérer les données du formulaire
          this.authService.register(userData).subscribe(
            (response) => {
              // Gérer la réponse du serveur en cas de succès
              console.log('Inscription réussie !', response);
              // Rediriger vers une autre page ou afficher un message de succès
            },
            (error) => {
              // Gérer les erreurs lors de l'appel au backend
              console.error('Erreur lors de l\'inscription :', error);
              // Message d'erreur à l'utilisateur
            }
          );
        } else {
          console.log(this.registerForm);
          alert('Formulaire invalide !');
        }
      }
}
