import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from "@angular/router";
import { AuthService } from "src/app/_services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}


  ngOnInit(): void {
    // Initialisation du formulaire de connexion
    this.loginForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(8)
      ]]
    });
  }

  get email() {
    return this.loginForm.get('email') as FormControl;
  }

  get password() {
    return this.loginForm.get('password') as FormControl;
  }

  LogIn() {
    if (this.loginForm.valid) {
      // Récupération des données du formulaire
      const userData = {
        email: this.email.value,
        password: this.password.value
      };

      // Appel au service d'authentification pour la connexion
      this.authService.login(userData).subscribe({
        next: (data) => {
          // Réussite de la connexion
          console.log('Connexion réussie !');
          // Simule un refresh sur le composant Login pour MAJ Header
          window.location.reload();
          // Redirection vers la page d'accueil
          this.router.navigate(['/']);
        },
        error: (error) => {
          // Erreur lors de la connexion
          console.error('Erreur de connexion :', error);
          alert('Erreur de connexion');
        }
    });
    } else {
      // Affichage d'une alerte si le formulaire est invalide
      alert('Formulaire invalide !');
      return;
    }
  }
}

