import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {

  forgotForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit(): void {
    this.forgotForm = this.formBuilder.group({
      email: ['', [
        Validators.email
      ]]
    })
  }

  get email() {
    return this.forgotForm.get('email') as FormControl;
  }

  onSubmit() {

    if(!this.forgotForm.valid) {
      return;
    }
  }
}
