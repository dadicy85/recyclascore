import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './_pages/auth/login/login.component';
import { RegisterComponent } from './_pages/auth/register/register.component';
import { ForgotPasswordComponent } from './_pages/auth/forgot-password/forgot-password.component';
import { httpInterceptorProviders } from './_helpers/http.interceptor';
import { CorsInterceptor } from './_helpers/cors.interceptor';
import { ButtonComponent } from './_components/button/button.component';
import { InputComponent } from './_components/input/input.component';
import { CheckboxComponent } from './_components/checkbox/checkbox.component';
import { BoardUserComponent } from './_pages/auth/board-user/board-user.component';
import { BoardAdminComponent } from './_pages/auth/board-admin/board-admin.component';
import { BoardModeratorComponent } from './_pages/auth/board-moderator/board-moderator.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ButtonComponent,
    InputComponent,
    CheckboxComponent,
    BoardUserComponent,
    BoardAdminComponent,
    BoardModeratorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [httpInterceptorProviders, CorsInterceptor],
  bootstrap: [AppComponent]
})
export class AppModule { }
