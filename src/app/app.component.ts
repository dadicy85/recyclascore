import { Component, OnInit } from '@angular/core';
import { ObjectService } from './_services/object.service';
import { StorageService } from './_services/storage.service';
import { AuthService } from './_services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'recyclascore';
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  showUserBoard = false;
  objectList: any[] = [];
  recyclabilityIndex!: number;
  username?: string;

  constructor(
    private objectService: ObjectService,
    private storageService: StorageService,
    private authService: AuthService
  ) {}

  onSelectObject(event: any): void {
    const selectedObjectName = event.target.value;
    console.log('Objet sélectionné :', selectedObjectName);

    this.objectService.getRecyclabilityIndex(selectedObjectName).subscribe(
      (index: number) => {
        this.recyclabilityIndex = index;
        console.log('Index de Recyclabilité :', this.recyclabilityIndex);
      },
      (error: any) => {
        console.error("Une erreur s'est produite :", error);
      }
    );
  }

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();

    if (this.isLoggedIn) {
      const token = this.storageService.getToken();

      const tokenDecoded: any = this.storageService.decodeToken(token as string);
      this.roles = tokenDecoded.roles;
      console.log(this.roles + " Mes roles");

      this.showAdminBoard = this.roles.includes('admin');
      this.showModeratorBoard = this.roles.includes('mod');
      this.showUserBoard = this.roles.includes('user');

      this.username = tokenDecoded.sub;
    }

    this.objectService.getObjects().subscribe(
      (objects: any[]) => {
        this.objectList = objects;
        console.log('Liste des objets :', this.objectList);
      },
      (error: any) => {
        console.error("Une erreur s'est produite lors de la récupération des objets :", error);
      }
    );
  }

  logout(): void {
    this.authService.logout().subscribe();
  }
}
